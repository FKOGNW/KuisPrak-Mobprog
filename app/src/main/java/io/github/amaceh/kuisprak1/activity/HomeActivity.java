package io.github.amaceh.kuisprak1.activity;

import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import io.github.amaceh.kuisprak1.R;
import io.github.amaceh.kuisprak1.activity.api.ApiClient;
import io.github.amaceh.kuisprak1.activity.api.ApiData;
import io.github.amaceh.kuisprak1.activity.api.ApiInterface;
import io.github.amaceh.kuisprak1.activity.model.Barang;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {

    private RecyclerView rcBarang;
    private BarangAdapter adapter;
    private ApiData<List<Barang>> apiBarang;

    private List<Barang> listBarang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        rcBarang = findViewById(R.id.rc_barang);

        listBarang = new ArrayList<Barang>();

        adapter = new BarangAdapter(this, listBarang);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        //layoutManager.setReverseLayout(true);
        //layoutManager.setStackFromEnd(true);
        adapter.notifyDataSetChanged();
        rcBarang.setLayoutManager(layoutManager);
        rcBarang.setAdapter(adapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rcBarang.getContext(),
                layoutManager.getOrientation());
        rcBarang.addItemDecoration(dividerItemDecoration);
        rcBarang.setHasFixedSize(true);
        rcBarang.invalidate();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.progressbar, null);
        builder.setView(view);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();

        ImageButton bt_tambah = findViewById(R.id.bt_tambah);

        bt_tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), KelolaActivity.class);
                startActivity(i);
            }
        });

        ImageButton btSync= findViewById(R.id.btSync);
        btSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                Call<ApiData<List<Barang>>> call = apiService.getAllBarang();
                call.enqueue(new Callback<ApiData<List<Barang>>>() {
                    @Override
                    public void onResponse(@NonNull Call<ApiData<List<Barang>>> call, @NonNull Response<ApiData<List<Barang>>> response) {
                        apiBarang = response.body();
                        dialog.dismiss();

                        if (apiBarang != null) {
//                            if (apiBarang.getStatus().equals("success")){
                                Toast.makeText(HomeActivity.this, "Sync Selesai", Toast.LENGTH_LONG).show();
                                listBarang.addAll(apiBarang.getData());
                                //adapter.notifyItemRangeInserted(0, apiKuliah.getData().size());
                                adapter.notifyDataSetChanged();
//                            }
//                            else{
//                                Toast.makeText(HomeActivity.this, "Something might be wrong", Toast.LENGTH_LONG).show();
//                                //dialog.dismiss();
//                                return;
//                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ApiData<List<Barang>>> call, @NonNull Throwable t) {
                        dialog.dismiss();
//                        Log.e(TAG, "onFailure: ", t);
                        Toast.makeText(HomeActivity.this, "connection error", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}

package io.github.amaceh.kuisprak1.activity.api;

import java.util.List;

import io.github.amaceh.kuisprak1.activity.model.Barang;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("barang/")
    Call<ApiData<List<Barang>>> getAllBarang();

    @FormUrlEncoded
    @POST("barang/")
    Call<ApiData<Barang>> postBarang(@Field("nama_barang") String nama_barang, @Field("jenis") String jenis,
                          @Field("harga") int harga);


    @FormUrlEncoded
    @PUT("barang/")
    Call<ApiData<Barang>> putBarang(@Field("id") int id,@Field("nama_barang") String nama_barang, @Field("jenis") String jenis,
                               @Field("harga") int harga);

    @FormUrlEncoded
    @DELETE("barang")
    Call<ApiData<Barang>> deleteBarang(@Path("id") int id);
}

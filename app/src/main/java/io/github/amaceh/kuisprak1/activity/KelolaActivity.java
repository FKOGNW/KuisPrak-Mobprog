package io.github.amaceh.kuisprak1.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import io.github.amaceh.kuisprak1.R;
import io.github.amaceh.kuisprak1.activity.api.ApiClient;
import io.github.amaceh.kuisprak1.activity.api.ApiData;
import io.github.amaceh.kuisprak1.activity.api.ApiInterface;
import io.github.amaceh.kuisprak1.activity.model.Barang;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KelolaActivity extends AppCompatActivity {

    private ApiData<Barang> account;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kelola);
        Spinner sp_jenis = findViewById(R.id.sp_jenis);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.l_jenis, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        sp_jenis.setAdapter(adapter);

        final String item= sp_jenis.getSelectedItem().toString();

        final EditText et_nama_barang, et_harga;

        et_nama_barang= findViewById(R.id.et_nama_barang);
        et_harga= findViewById(R.id.et_harga);


        Button button= findViewById(R.id.button);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.progressbar, null);
        builder.setView(view);
        builder.setCancelable(false);
        final Dialog dialog = builder.create();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String harga= (et_harga.getText().toString());

                final int hargaF= Integer.parseInt(harga);
                final String barang= et_nama_barang.getText().toString();
                dialog.show();
                final View.OnClickListener c = this;
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                Call<ApiData<Barang>> call = apiService.postBarang(barang
                        ,item, hargaF);
                call.enqueue(new Callback<ApiData<Barang>>() {
                    @Override
                    public void onResponse(@NonNull Call<ApiData<Barang>> call, @NonNull Response<ApiData<Barang>> response) {
                        account = response.body();
                        dialog.dismiss();

                        if (account != null) {
                            if (account.getStatus().equals("success")){
                                Toast.makeText(KelolaActivity.this, "post done", Toast.LENGTH_LONG).show();
                                finish();
                                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                            }
                            else{
                                Toast.makeText(KelolaActivity.this, "wrong Email or Password", Toast.LENGTH_LONG).show();
                                //dialog.dismiss();
                                return;
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ApiData<Barang>> call, @NonNull Throwable t) {
                        dialog.dismiss();
//                Log.e(TAG, "onFailure: ", t);
                        Toast.makeText(KelolaActivity.this, "connection error", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });


    }
}

package io.github.amaceh.kuisprak1.activity;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import io.github.amaceh.kuisprak1.R;
import io.github.amaceh.kuisprak1.activity.model.Barang;

public class BarangAdapter extends RecyclerView.Adapter<BarangAdapter.ViewHolder>{

    private List<Barang> mData;
    private LayoutInflater mInflater;
    private int position;

    public BarangAdapter(Context context, List<Barang> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }


    @NonNull
    @Override
    public BarangAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_barang, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final BarangAdapter.ViewHolder holder, int position) {
        final Barang barang = mData.get(position);
        holder.id = barang.getId();
        holder.tvNama.setText(barang.getNama_barang());
        holder.tvJenis.setText(barang.getJenis());
        holder.tvHarga.setText((barang.getHarga()+""));
//        holder.matkul.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(mInflater.getContext(), "Detail kode "+holder.kode_mk, Toast.LENGTH_SHORT).show();
//            }
//        });
        holder.barang.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                PopupMenu popup = new PopupMenu(mInflater.getContext(), view);
                popup.getMenuInflater()
                        .inflate(R.menu.barang_menu, popup.getMenu());

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.mn_edit :
                                Intent in = new Intent(mInflater.getContext(), KelolaActivity.class);
                                in.putExtra("ID",holder.id);
                                mInflater.getContext().startActivity(in);
                                Toast.makeText(mInflater.getContext(), "edit "+holder.id, Toast.LENGTH_SHORT).show();
                                return true;
                            case R.id.mn_delete :
                                Toast.makeText(mInflater.getContext(), "hapus "+holder.id, Toast.LENGTH_SHORT).show();

//                                SharedPreferences session= = getSharedPreferences("io.github.amaceh.backend_prak.user", MODE_PRIVATE);
//                                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
//
//                                Call<ApiData<Kuliah>> call= apiService.deleteMatkul(holder.kode_mk,);
//
//
//                                call.enqueue(new Callback<ApiData<Kuliah>>() {
//                                    @Override
//                                    public void onResponse(Call<ApiData<Kuliah>> call, Response<ApiData<Kuliah>> response) {
//                                    }
//
//                                    @Override
//                                    public void onFailure(Call<ApiData<Kuliah>> call, Throwable t) {
//                                        return;
//                                    }
//                                });
                                return true;
                            default:
                                return true;
                        }
                    }

                });
                popup.show(); //showing popup menu
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public int id;
        public TextView tvNama, tvJenis, tvHarga;
        public ConstraintLayout barang;

        public ViewHolder(View itemView) {
            super(itemView);
            barang = itemView.findViewById(R.id.list_barang);
            tvNama = itemView.findViewById(R.id.tvNama);
            tvJenis = itemView.findViewById(R.id.tvJenis);
            tvHarga = itemView.findViewById(R.id.tvHarga);
        }
    }
}

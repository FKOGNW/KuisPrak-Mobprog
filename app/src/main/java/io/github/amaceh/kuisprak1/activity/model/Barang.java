package io.github.amaceh.kuisprak1.activity.model;

public class Barang {

    int id, harga;
    String nama_barang, jenis;

    public Barang(){}

    public Barang(int id, int harga, String nama_barang, String jenis) {
        this.id = id;
        this.harga = harga;
        this.nama_barang = nama_barang;
        this.jenis = jenis;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }
}
